use anpp::Packet;
use std::{
    cmp, env,
    io::{self, Read},
};

fn main() {
    let id = env::args().nth(1).unwrap_or_else(|| String::from("42"));
    let id = id
        .parse()
        .expect("The ID must be an integer between 0 and 255");

    let mut buffer = Vec::with_capacity(Packet::MAX_PACKET_SIZE);
    let bytes_written = io::stdin().read_to_end(&mut buffer).unwrap();

    if bytes_written > Packet::MAX_PACKET_SIZE {
        eprintln!(
            "Warning: Input string should be less than {} bytes long",
            Packet::MAX_PACKET_SIZE
        );
    }

    let len = cmp::min(Packet::MAX_PACKET_SIZE + 1, bytes_written);
    let data = &buffer[..len];

    let packet = Packet::with_data(id, data).unwrap();

    let mut encoded = vec![0; 512];
    let bytes_written = packet.write_to_buffer(&mut encoded).unwrap();
    let encoded = &encoded[..bytes_written];

    println!("Header: {:x?}", &encoded[..5]);
    println!("Body: {:x?}", &encoded[5..]);
}
