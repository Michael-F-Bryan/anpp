extern crate anpp;
#[macro_use]
extern crate criterion;

use anpp::{utils, Decoder, Packet};
use criterion::{Criterion, ParameterizedBenchmark, Throughput};

fn sizes() -> Vec<u32> { vec![1, 32, 127, 255] }

fn random_packet(size: u32) -> Packet {
    let bytes: Vec<u8> = (0..size as u8).collect();
    Packet::with_data(42, &bytes).unwrap()
}

fn decoder_with_random_packet(size: u32) -> Decoder {
    let pkt = random_packet(size);
    let mut buffer = vec![0; pkt.total_length()];
    pkt.write_to_buffer(&mut buffer).unwrap();

    let mut decoder = Decoder::new();
    decoder.push_data(&buffer).unwrap();
    decoder
}

fn generate_header(c: &mut Criterion) {
    c.bench(
        "anpp",
        ParameterizedBenchmark::new(
            "generate-header",
            |b, &len| {
                let pkt = random_packet(len);
                let mut buffer = vec![0; pkt.total_length()];

                b.iter(|| pkt.write_to_buffer(&mut buffer).unwrap())
            },
            sizes(),
        )
        .throughput(|&len| Throughput::Bytes(len)),
    );
}

fn decode_packet(c: &mut Criterion) {
    c.bench(
        "anpp",
        ParameterizedBenchmark::new(
            "decode-packet",
            |b, &len| {
                let dec = decoder_with_random_packet(len);

                b.iter(|| dec.clone().decode().unwrap())
            },
            sizes(),
        )
        .throughput(|&len| Throughput::Bytes(len)),
    );
}

fn utility_functions(c: &mut Criterion) {
    let inputs = vec![0, 10, 100, 1000];

    c.bench(
        "utils",
        ParameterizedBenchmark::new(
            "crc16",
            |b, &len| {
                let data: Vec<u8> = (0..len as u8).collect();
                b.iter(|| utils::calculate_crc16(&data))
            },
            inputs,
        )
        .throughput(|&len| Throughput::Bytes(len)),
    );
}

fn crc_vs_header_gen(c: &mut Criterion) {
    c.bench(
        "crc-vs-header-gen",
        ParameterizedBenchmark::new(
            "generate-header",
            |b, &len| {
                let pkt = random_packet(len);
                let mut buffer = vec![0; pkt.total_length()];
                b.iter(|| pkt.write_to_buffer(&mut buffer).unwrap());
            },
            sizes(),
        )
        .with_function("crc", |b, &len| {
            let data: Vec<u8> = (0..len as u8).collect();
            b.iter(|| utils::calculate_crc16(&data))
        })
        .throughput(|&len| Throughput::Bytes(len)),
    );
}

criterion_group! { benches, generate_header, decode_packet, utility_functions, crc_vs_header_gen }

criterion_main!(benches);
